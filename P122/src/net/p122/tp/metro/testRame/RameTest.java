package net.p122.tp.metro.testRame;

import java.rmi.Naming;

import net.p122.tp.metro.metroorchestre.Rame;

public class RameTest{

	public static void main(String[] args) {
		try {
			
			Rame rame = (Rame) Naming.lookup("rmi://localhost:9000/Rame1");

			System.out.println("[CLIENT] Appel méthode");
			rame.departImminent();
			
			rame.demarrer();
			
			rame.fermerPorte();
			
			rame.actionnerMoteur(500);
			System.out.println("[CLIENT] fin");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
