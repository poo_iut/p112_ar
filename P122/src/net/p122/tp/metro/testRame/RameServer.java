package net.p122.tp.metro.testRame;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;

import net.p122.tp.metro.metroorchestre.Rame;
import net.p122.tp.metro.metroorchestre.RameImpl;

public class RameServer {

	public static void main(String args[]) {
		int rameCount = 1;

		try {
			
			//LocateRegistry.createRegistry(9000);
			Rame rame = new RameImpl(rameCount++);
			Naming.bind("rmi://localhost:9000/Rame1", rame);
			
			System.out.println("[SERVER] Rame "+rame.getNumero());
			
		} catch (RemoteException | MalformedURLException | AlreadyBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		new Thread(() -> {
			while(true) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
	
}
