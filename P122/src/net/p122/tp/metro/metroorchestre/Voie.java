package net.p122.tp.metro.metroorchestre;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class Voie {

	private Rame laRame;
	private int temps;
	private boolean feuVert;
	private Station stationSuivante;
	private int voieSuivante;

	public Voie(int temps) {
		this.temps = temps;
		this.feuVert = true;
	}

	private class GestionnaireDeRame extends Thread{
		
		@Override
		public void run() {
			// Récupérer la rame sur la voie
			Rame rame = getRame();
			if(rame == null) {
				return;
			}
			try {
				// Démarrer la rame
				rame.demarrer();
				// Fermer les portes
				rame.fermerPorte();
				// Supprimer la rame de la voie
				Voie.this.laRame = null;
				// Actionner le moteur
				rame.actionnerMoteur(Voie.this.temps);
				// Allumer le feu vert
				allumerFeuVert();
				// Positionner la rame sur la nouvelle voie
				Voie.this.stationSuivante.setRame(Voie.this.voieSuivante, rame);
				// Ouvrir les portes
				rame.ouvrirPorte();
				// On fait dodo pendant 1000 ms
				Thread.sleep(Superviseur.WAIT_TIME);
				// Activer le signal départ imminent
				rame.departImminent();
			} catch (RemoteException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void demarrerRame() {
		try {
			this.stationSuivante.allumerFeuRouge(getNumeroVoieSuivante());
			new GestionnaireDeRame().start();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean estVert() {
		return this.feuVert;
	}
	
	public void allumerFeuRouge() {
		this.feuVert = false;
	}

	public void allumerFeuVert() {
		this.feuVert = true;
	}


	public void setRame(Rame rame) throws RemoteException {
		if(this.laRame != null && rame != null) {
			throw new RemoteException();
		}
		this.laRame = rame;
	}
	
	public Rame getRame() {
		return this.laRame;
	}

	public boolean estRamePresente(Rame rame) throws RemoteException {
		if(this.laRame == null || rame == null)return false;
		return this.laRame.getNumero() == rame.getNumero();
	}
	
	public String getNomStationSuivante() throws RemoteException {
		return this.stationSuivante.getNom();
	}
	
	public int getNumeroVoieSuivante() throws Exception{
		return this.voieSuivante;
	}

	
	public void setStationSuivante(Station stationSuivante) {
		this.stationSuivante = stationSuivante;
	}
	
	@Override
	public String toString() {
		String rame = "null";
		if(this.laRame != null) {
			try {
				rame = String.valueOf(this.laRame.getNumero());
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		return "Voie [feuVert=" + feuVert + ", voieSuivante=" + voieSuivante +", rame="+rame+"]";
	}

	public void ajouterStationSuivante(String host, int port, String name, int voieSuivante) {
		try {
			this.stationSuivante =
					(Station) Naming.lookup("rmi://"+host+":"+port+"/"+name);
			this.voieSuivante = voieSuivante;
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
	}

	public void setVoieSuivante(int voieSuivante2) {
		this.voieSuivante = voieSuivante2;
	}
	
	
	

	
}
