package net.p122.tp.metro.metroorchestre;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

public class StationServer {

	
	public static void main() {
		try {
			LocateRegistry.createRegistry(Superviseur.PORT);
			
			for(int i = 0; i < Station.STATIONS.length; i++) {
				
				String nomStation = Station.STATIONS[i];
				int numeroStation = i+1;
				Station station = new StationImpl(nomStation, 2);
				Naming.bind("rmi://localhost:"+Superviseur.PORT+"/St"+numeroStation, station);
			}
			
			Station depot = new DepotImpl("Depôt");
			Naming.bind("rmi://localhost:"+Superviseur.PORT+"/Depot", depot);
			for(int i = 0; i < Superviseur.NB_RAME; i++) {
				Rame rame = new RameImpl(i+1);
				Naming.bind("rmi://localhost:"+Superviseur.PORT+"/Rame"+(i+1), rame);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
}
