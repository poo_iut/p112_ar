package net.p122.tp.metro.metroorchestre;

import java.rmi.RemoteException;
import java.util.Deque;
import java.util.LinkedList;

public class DepotImpl extends StationImpl implements Station{

	/**
	 * @author Fabien CAYRE
	 *
	 * 2020-03-26
	 */
	private LinkedList<Rame> parking;
	
	public DepotImpl(String nomStation) throws RemoteException {
		super(nomStation, 1);
		super.lesVoies.get(0).allumerFeuVert();
		this.parking = new LinkedList<Rame>();
	}
	
	@Override  
	public void allumerFeuRouge(int numeroVoie) throws RemoteException {}
	
	@Override
	public boolean estFeuVert(int numeroVoie) throws RemoteException {
		return true;
	}
	
	@Override
	public void demarrerRame(int numeroVoie) throws RemoteException {
		if(numeroVoie != 1)return;
		// On récupère la première occurence de la liste ET on supprime l'occurence de la liste
		Rame premiereRame = this.parking.pollFirst();
		if(premiereRame != null) {
			if(super.lesVoies.get(0).getRame() == null) {
				// On place la rame sur la voie
				super.setRame(numeroVoie, premiereRame);
				try {
					// Départ imminent
					premiereRame.departImminent();
				} catch (RemoteException | InterruptedException e) {
					e.printStackTrace();
				}
				super.demarrerRame(numeroVoie);
			}
		}
	}
	
	@Override
	public int getNumeroVoie(Rame rame) throws RemoteException {
		if(this.parking.contains(rame) && rame.estRamePreteAPartir()) {
			return 1;
		}
		if(super.getNumeroVoie(rame) == 1) {
			return 1;
		}
		return -1;
	}

	@Override
	public void setRame(int numeroVoie, Rame rame) throws RemoteException {
		if(numeroVoie == 1) {
			this.parking.addLast(rame);
		}
	}
	
}
