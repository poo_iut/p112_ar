package net.p122.tp.metro.metroorchestre;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.p122.tp.metro.afficheurmetro.MetroSuperviseurIHM;

public class StationImpl extends UnicastRemoteObject implements Station {

	protected List<Voie> lesVoies;
	protected Map<Integer, Integer> voieVersVoieSuivante;
	protected Map<Integer, String> voieVersStationSuivante;
	protected Map<Integer, String> voieVersStationSuivanteMaisJusteLeVraiNomDeLaStation;
	protected String nomStation;

	/**
	 * 
	 * @param nomStation
	 * @param nbVoies    nombre de voies
	 * @throws RemoteException
	 */
	public StationImpl(String nomStation, int nbVoies) throws RemoteException {
		super();
		this.lesVoies = new ArrayList<Voie>(nbVoies);

		// Rempli les voies
		for (int i = 0; i < nbVoies; i++) {
			this.lesVoies.add(new Voie(500));
		}
		this.voieVersVoieSuivante = new HashMap<Integer, Integer>();
		this.voieVersStationSuivante = new HashMap<Integer, String>();
		this.voieVersStationSuivanteMaisJusteLeVraiNomDeLaStation = new HashMap<Integer, String>();
		this.nomStation = nomStation;
	}

	public StationImpl() throws RemoteException {
		super();
	}

	public StationImpl(int port, RMIClientSocketFactory csf, RMIServerSocketFactory ssf) throws RemoteException {
		super(port, csf, ssf);
		this.voieVersVoieSuivante = new HashMap<Integer, Integer>();
		this.voieVersStationSuivante = new HashMap<Integer, String>();
		this.voieVersStationSuivanteMaisJusteLeVraiNomDeLaStation = new HashMap<Integer, String>();
	}

	public StationImpl(int port) throws RemoteException {
		super(port);
		this.voieVersVoieSuivante = new HashMap<Integer, Integer>();
		this.voieVersStationSuivante = new HashMap<Integer, String>();
		this.voieVersStationSuivanteMaisJusteLeVraiNomDeLaStation = new HashMap<Integer, String>();
	}

	@Override
	public String afficher() throws RemoteException {
		return new StringBuilder().append("NomStation : " + this.nomStation).append("\n")
				.append("Voies Vers Voie Suivantes : " + this.voieVersVoieSuivante).append("\n")
				.append("Voie Vers Station Suivantes : " + this.voieVersStationSuivante).append("\n").toString();
	}

	@Override
	public String getNom() throws RemoteException {
		return this.nomStation;
	}

	@Override
	public void ajouterStationSuivante(String machine, int port, int numeroVoieDepart, String stationSuivante,
			int voieSuivante) throws RemoteException, NotBoundException, MalformedURLException {
		Station stationSuivant = null;
		try {
			stationSuivant = (Station) Naming.lookup("rmi://" + machine + ":" + port + "/" + stationSuivante);
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
		this.voieVersStationSuivante.put(numeroVoieDepart, stationSuivante);
		this.lesVoies.get(numeroVoieDepart - 1).setStationSuivante(stationSuivant);
		this.lesVoies.get(numeroVoieDepart - 1).setVoieSuivante(voieSuivante);
		this.voieVersVoieSuivante.put(numeroVoieDepart, voieSuivante);
		this.voieVersStationSuivanteMaisJusteLeVraiNomDeLaStation.put(numeroVoieDepart, stationSuivant.getNom());
	}

	@Override
	public boolean estFeuVert(int numeroVoie) throws RemoteException {
		return this.lesVoies.get(numeroVoie - 1).estVert();
	}

	@Override
	public void demarrerRame(int numeroVoie) throws RemoteException {
		// Récupérer le numéro de la rame
		Voie voie = this.lesVoies.get(numeroVoie - 1);
		Rame rame = voie.getRame();
		// Démarrer la rame
		voie.demarrerRame();

		try {
			// Récupérer la référence vers le superviseurIHM nommé moniteur qui est un objet
			// distribué
			MetroSuperviseurIHM superViseur = (MetroSuperviseurIHM) Naming.lookup("rmi://localhost:9999/moniteur");
			String nomCourrant = getNom();
			String nomSuivant = this.voieVersStationSuivanteMaisJusteLeVraiNomDeLaStation.get(numeroVoie);
			if(nomCourrant.equals("Depôt")) {
				nomCourrant = nomSuivant;
			}
			
			//Thread.sleep(3000);
			superViseur.modifierAffichage(
					nomCourrant, numeroVoie,
					nomSuivant, getNumeroVoieSuivante(numeroVoie),
					rame.getNumero());
			
		} catch (MalformedURLException | NotBoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public String getNomStationSuivante(int numeroVoie) throws RemoteException {
		return this.voieVersStationSuivante.get(numeroVoie);
	}

	@Override
	public int getNumeroVoieSuivante(int numeroVoie) throws RemoteException {
		int voie = this.voieVersVoieSuivante.get(numeroVoie);
		return voie;
	}

	@Override
	public void setRame(int numeroVoie, Rame rame) throws RemoteException {
		if(numeroVoie <= 0 )return;
		if(this.lesVoies.get(numeroVoie - 1) == null) return;
		this.lesVoies.get(numeroVoie - 1).setRame(rame);
	}

	@Override
	public void allumerFeuRouge(int numeroVoie) throws RemoteException {
		if(numeroVoie <= 0 )return;
		if(this.lesVoies.get(numeroVoie - 1) == null) return;
		this.lesVoies.get(numeroVoie - 1).allumerFeuRouge();
	}

	@Override
	public int getNumeroVoie(Rame rame) throws RemoteException {
		for (int i = 0; i < this.lesVoies.size(); i++) {
			Voie voie = this.lesVoies.get(i);
			if (voie.estRamePresente(rame)) {
				return i + 1;
			}
		}
		return -1;
	}

}
