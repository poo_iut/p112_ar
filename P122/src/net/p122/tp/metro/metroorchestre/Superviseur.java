package net.p122.tp.metro.metroorchestre;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import net.p122.tp.metro.afficheurmetro.EcouteurBoutons;
import net.p122.tp.metro.afficheurmetro.MetroSuperviseurIHM;
import net.p122.tp.metro.afficheurmetro.MetroSuperviseurIHMImpl;

/**
 *
 * @author millan
 */
public class Superviseur implements EcouteurBoutons {

	public static final int PORT = 9000;
	public static final int NB_RAME = 3;
	public static final int WAIT_TIME = 1000;

	public static void main(String[] args) {
		StationServer.main();
		new Superviseur();
	}

	private enum ETAT {
		DEMARRAGE_METRO, EN_SERVICE, FIN_DE_SERVICE
	};

	private GestionMetro metro;
	private ETAT etat;
	private boolean toutesRamesDansDepot;
	
	public Superviseur() {
		this.metro = new GestionMetro();
		this.etat = ETAT.FIN_DE_SERVICE;
		this.toutesRamesDansDepot = false;

		// Construction du metro
		try {

			Station depot = rechercherStation("Depot");
			depot.ajouterStationSuivante("localhost", PORT, 1, "St1", 1);
			for (int i = 1; i < Station.STATIONS.length; i++) {
				// St1 -> St2 -> St3 ....
				if(i == 0) {
					System.err.println("L'index est de 0");
					System.exit(-1);
				}
				Station stationActuelle = rechercherStation("St" + i);
				Station prochaine = rechercherStation("St"+(i+1));
				System.out.println(stationActuelle.getNom()+" --> "+prochaine.getNom());
				stationActuelle.ajouterStationSuivante("localhost", PORT, 1, "St"+(i+1), 1);
				// Opposé
				// St9 -> St8 -> St7 ...
				int oppIndex = Station.STATIONS.length - i;
				if(oppIndex > 0) {
					Station stationOpposee = rechercherStation("St" + (oppIndex+1));
					stationOpposee.ajouterStationSuivante("localhost", PORT, 2, "St" + (oppIndex), 2);
				}
			}
			String nomDerniere = "St" + (Station.STATIONS.length);
			Station derniere = rechercherStation(nomDerniere);
			derniere.ajouterStationSuivante("localhost", PORT, 1, nomDerniere, 2);

			String nomPremiere = "St1";
			Station premiere = rechercherStation(nomPremiere);
			premiere.ajouterStationSuivante("localhost", PORT, 2, nomPremiere, 1);

			try {
				for (int i = 0; i < NB_RAME; i++) {
					depot.setRame(1, rechercherRame(i + 1));
				}
				Station suiteDepot = rechercherStation(depot.getNomStationSuivante(1));
				System.out.println("Suite au dépot  : "+suiteDepot.getNom());
			} catch (ClassCastException e) {
				e.printStackTrace();
				System.exit(-1);
			}
			MetroSuperviseurIHM ihm = new MetroSuperviseurIHMImpl(this, Station.STATIONS);
			LocateRegistry.createRegistry(9999);
			Naming.bind("rmi://localhost:9999/moniteur", ihm);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class GestionMetro extends Thread {

		public GestionMetro() {
			setDaemon(true);
		}

		@Override
		public void run() {
			try {
				Superviseur.this.etat = ETAT.DEMARRAGE_METRO;
				final Station depot = Superviseur.this.rechercherStation("Depot");
				System.out.println("[DEBUG] Début du while loop");
				while (!Superviseur.this.toutesRamesDansDepot) {
					for (int i = 0; i < NB_RAME; i++) {
						Rame rame = rechercherRame(i + 1);
						System.out.println("==============================");
						System.out.println("Rame : "+rame.getNumero());
						Thread.sleep(Superviseur.WAIT_TIME);
						if (rame.estRamePreteAPartir() &&
								(Superviseur.this.etat != ETAT.FIN_DE_SERVICE)
								|| depot.getNumeroVoie(rame) == -1) {
							Station actuelle = rechercherStation(rame);
							if(actuelle == null) {
								continue;
							}
							int voieActuelle = actuelle.getNumeroVoie(rame);
							Station suivante = rechercherStation(actuelle.getNomStationSuivante(voieActuelle));
							int voieSuivante = actuelle.getNumeroVoieSuivante(voieActuelle);
							
							if(suivante.estFeuVert(voieSuivante)) {
								actuelle.demarrerRame(voieActuelle);
							}
						}
						if(Superviseur.this.etat == ETAT.DEMARRAGE_METRO) {
							if(estDepotVide()) {
								Superviseur.this.etat = ETAT.EN_SERVICE;
							}
						}else if(Superviseur.this.etat == ETAT.FIN_DE_SERVICE){
							Superviseur.this.toutesRamesDansDepot = sontToutesLesRamesAuDepot();
						}
					}
				}
				System.out.println("la fin");
			} catch (MalformedURLException | RemoteException | NotBoundException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private void gererLigne() {
		new GestionMetro().start();
	}

	private void arreterMetro() {
		this.etat = ETAT.FIN_DE_SERVICE;
		try {
			String nomPremiere = "St1";
			Station premiere = rechercherStation(nomPremiere);
			premiere.ajouterStationSuivante("localhost", PORT, 2, "Depot", 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean sontToutesLesRamesAuDepot() {
		try {
			Station station = rechercherStation("Depot");
			for (int i = 0; i < Superviseur.NB_RAME; i++) {
				Rame rame = rechercherRame(i + 1);
				if (station.getNumeroVoie(rame) == -1) {
					return false;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public boolean estDepotVide() {
		try {
			Station station = rechercherStation("Depot");
			for (int i = 0; i < Superviseur.NB_RAME; i++) {
				Rame rame = rechercherRame(i + 1);
				if (station.getNumeroVoie(rame) == 1) {
					return false;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public Station rechercherStation(Rame rame) throws MalformedURLException, RemoteException, NotBoundException {
		Station depot = rechercherStation("Depot");
		if(depot.getNumeroVoie(rame) == 1)return depot;
		for (int i = 1; i <= Station.STATIONS.length; i++) {
			Station stationActuelle = rechercherStation("St" + i);
			if(stationActuelle.getNumeroVoie(rame) != -1)return stationActuelle;
		}
		return null;
	}

	public Station rechercherStation(String nomStation)
			throws MalformedURLException, RemoteException, NotBoundException {
		return (Station) Naming.lookup("rmi://localhost:" + PORT + "/" + nomStation);
	}

	public Rame rechercherRame(int numRame) throws MalformedURLException, RemoteException, NotBoundException {
		return (Rame) Naming.lookup("rmi://localhost:" + PORT + "/Rame" + String.valueOf(numRame));
	}

	/**
	 * actionEcouteurDemarrer et actionEcouteurArreter. <B>Ces méthodes sont
	 * fournies aux étudiants.</B>
	 */
	@Override
	public void actionEcouteurDemarrer() {
		Superviseur.this.gererLigne();
	}

	@Override
	public void actionEcouteurArreter() {
		Superviseur.this.arreterMetro();
	}

}