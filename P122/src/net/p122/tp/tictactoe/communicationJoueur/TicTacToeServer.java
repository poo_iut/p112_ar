package net.p122.tp.tictactoe.communicationJoueur;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import net.p122.tp.tictactoe.tictactoe.GestionClicCase;
import net.p122.tp.tictactoe.tictactoe.TicTacToeIHM;
import net.p122.tp.tictactoe.tictactoe.TicTacToeIHMImpl;

public class TicTacToeServer {

	private static int LOCAL_PORT;
	private static String LOCAL_ADDR;
	private static int REMOTE_PORT;
	private static String REMOTE_ADDR;
	
	public static void main(String[] args) {
		if(args.length != 5) {
			System.out.println("Veuillez saisir 5 arguments");
			return;
		}
		
		String disp = 
				  "\n Port local : %d, Adresse Locale : %s "
				+ "\n Port Distant : %d, Adresse Distante : %s"
				+ "\n Est Premier : %b";
		
		LOCAL_PORT = Integer.parseInt(args[0]);
		LOCAL_ADDR = args[1];
		REMOTE_ADDR = args[2];
		REMOTE_PORT = Integer.parseInt(args[3]);
		boolean estPremier = Boolean.parseBoolean(args[4]);
		
		System.out.println(String.format(disp, LOCAL_PORT, LOCAL_ADDR,
										 REMOTE_PORT, REMOTE_ADDR, estPremier));
		
		try {
			TicTacToeIHM ihm = new TicTacToeIHMImpl(estPremier);
			
			new TicTacToeServer(new TicTacToeImpl(ihm, REMOTE_ADDR, REMOTE_PORT, estPremier),
								LOCAL_PORT,
								ihm);
			
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
	}
	
	public TicTacToeServer(TicTacToe ticTacToeImpl, int port, TicTacToeIHM ihm) throws RemoteException {
		LocateRegistry.createRegistry(port);
		System.out.println("Registered on :" + port);
		try {
			Naming.bind("rmi://"+LOCAL_ADDR+":"+port+"/joueur", ticTacToeImpl);
		} catch (MalformedURLException | AlreadyBoundException e) {
			e.printStackTrace();
		}
		ihm.setGestionnaireClic((GestionClicCase) ticTacToeImpl);
	}
	
}