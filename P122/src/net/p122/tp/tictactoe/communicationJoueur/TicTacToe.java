package net.p122.tp.tictactoe.communicationJoueur;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TicTacToe extends Remote, Serializable{
	
	
	void propagerCoup(int ligne, int colonne) throws RemoteException;
	
}