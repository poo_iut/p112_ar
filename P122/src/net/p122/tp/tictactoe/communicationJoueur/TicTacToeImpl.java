package net.p122.tp.tictactoe.communicationJoueur;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import net.p122.tp.tictactoe.tictactoe.GestionClicCase;
import net.p122.tp.tictactoe.tictactoe.TicTacToeIHM;

public class TicTacToeImpl extends UnicastRemoteObject implements TicTacToe, GestionClicCase{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private TicTacToeIHM ihm;
	private boolean bloque;
	private String remoteAddr;
	private int remotePort;
	
	public TicTacToeImpl(TicTacToeIHM ihm, String addrRemote, int portRemote, boolean estPremier) throws RemoteException {
		super();
		this.ihm = ihm;
		this.remoteAddr = addrRemote;
		this.remotePort = portRemote;
	}
	
	@Override
	public void propagerCoup(int ligne, int colonne) throws RemoteException{
		System.out.println("Le coup ! "+ligne+" : "+colonne+" viens d'ailleurs");
		this.ihm.jouerDistant(ligne, colonne);
		this.bloque = false;
	}

	@Override
	public boolean traitementCaseCliquee(int lig, int col){
		if(this.ihm.getContenuCase(lig, col).isEmpty()
				&& !this.bloque) {
			try {
				String rmiAddr = "rmi://"+this.remoteAddr+":"+this.remotePort+"/joueur";
				System.out.println("Adresse remote : "+rmiAddr);
				((TicTacToe) Naming.lookup(rmiAddr))
						.propagerCoup(lig, col);;
			} catch (MalformedURLException | RemoteException | NotBoundException e) {
				e.printStackTrace();
			}
			this.bloque = true;
			return true;
		}
		return false;
	}
	
}
